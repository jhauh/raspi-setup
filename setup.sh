#!/bin/bash

## packages
apt-get update && apt-get upgrade -y

rpi-update

apt install -y $(cat package_list.txt)


# users
NEWUSER=$1
adduser $NEWUSER

for GROUP in $(groups pi | sed 's/.*:\spi//'); do 
    adduser $NEWUSER $GROUP; 
done

## log on as new user and delete pi
deluser pi

nano /etc/sudoers.d/010_pi-nopasswd
#jhauh ALL=(ALL) PASSWD: ALL

## set static IP and hostname
hostname -I # to get IP
nano /etc/dhcpcd.conf
# interface eth0
# static ip_address /24
# static routers
# static domain_name_servers
sudo nano /etc/hostname
sudo nano /etc/hosts

# tighten up ssh
ssh-copy-id -i .ssh/id_cr.pub jhauh@...
sudo nano /etc/ssh/sshd_config
# AllowUsers $NEWUSER
# UsePAM no
# PermitRootLogin no
# ChallengeResponseAuthentication no
# PasswordAuthentication no

# configure fail2ban
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
# maxretry = 4
# bantime = 60000

