Raspi setup
===========

*Most* of the time you start out on a new project with your raspberry pi you'll use Raspbian as the base. The files in here get you through most of the boilerplate:

#. Updating the system
#. Adding new users and removing the ``pi`` user
#. Setting a static IP
#. Tightening up ssh access

At this point, ``setup.sh`` is **not** runnable as a script, as it requires making some changes to root files using nano, and I haven't figured out a reliable way to do some of it yet. So look at it as more of a guideline.

One day I'll do something sensible with this like turn it into a container, or start using someone else's containers or deployment tools. But it is not this day!